package com.automation.qa.travelocity.questions;

import com.automation.qa.travelocity.user_interfaces.TraveloCity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ResultVuelo implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {


        return Text.of( TraveloCity.resultVuelo).viewedBy(actor).asString();


    }
    public static Result is_vuelo() {
        return new Result();
    }
}
