package com.automation.qa.travelocity.questions;

import com.automation.qa.travelocity.user_interfaces.TraveloCity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;

public class Result implements Question<String> {


    @Override
    public String answeredBy(Actor actor) {


            return Text.of(TraveloCity.resultHotels).viewedBy(actor).asString();


    }


    public static Result is() {
        return new Result();
    }

}
