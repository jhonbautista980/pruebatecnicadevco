package com.automation.qa.travelocity.tasks;

import com.automation.qa.travelocity.interations.SelectDay;
import com.automation.qa.travelocity.user_interfaces.TraveloCity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

public class CosultaHoteles implements Task {

    private String destino;
    private String diaInicio;
    private String diaFin;
    private int adultos;
    private int ninos;
    private String salidaDesde;

    public CosultaHoteles(String destino, String diaInicio, String diaFin, int adultos, int ninos, String salidaDesde) {

        this.destino = destino;
        this.diaInicio = diaInicio;
        this.diaFin = diaFin;
        this.adultos = adultos;
        this.ninos = ninos;
        this.salidaDesde = salidaDesde;
    }


    @Override
    public <T extends Actor> void performAs(T actor) {


        actor.attemptsTo(
                SendKeys.of(destino).into(TraveloCity.txtDestino),
                Click.on(TraveloCity.selectDestino),
                Click.on(TraveloCity.btnDateInit),SelectDay.day(diaInicio, "Hoteles"),SelectDay.day(diaFin,"Hoteles"),
                Click.on(TraveloCity.btnDoneFechas),
                Click.on(TraveloCity.btnViajeros));
        for (int i = 1; i < adultos; i++) {
            actor.attemptsTo(Click.on(TraveloCity.agregarAdulto));
        }
        for (int i = 0; i < ninos; i++) {
            actor.attemptsTo(Click.on(TraveloCity.agregarNino));
        }
        actor.attemptsTo(Click.on(TraveloCity.doneViajeros),
                Click.on(TraveloCity.buscar));

    }

    public static CosultaHoteles cosulta(String destino, String diaInicio, String diaFin, int adultos, int ninos, String salidaDesde) {

        return Tasks.instrumented(CosultaHoteles.class, destino, diaInicio ,diaFin,adultos, ninos, salidaDesde);
    }
}
