package com.automation.qa.travelocity.interations;

import com.automation.qa.travelocity.user_interfaces.TraveloCity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;

public class SelectDay implements Interaction {
    private String day;
    private String tipReserve;

    public SelectDay(String day, String tipReserve) {
        this.day = day;
        this.tipReserve = tipReserve;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        Target element = null;

        switch (tipReserve){
            case "Hoteles":
                element = TraveloCity.selectDayInit;
                break;

            case "Vuelos":
                element = TraveloCity.selectDayInitVuelos;
                break;

        }
           actor.attemptsTo(Click.on(element.of(day)));

    }

    public static SelectDay day(String day, String tipReserve){
        return Tasks.instrumented(SelectDay.class, day, tipReserve);
    }
}
