package com.automation.qa.travelocity.user_interfaces;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class TraveloCity {

    public static final Target txtDestino = Target.the("txtDestino").locatedBy("//*[@id=\"location-field-destination-menu\"]/div[1]/button");
    public static final Target selectDayInit = Target.the("{0}").locatedBy("//*[@id=\"wizard-hotel-pwa-v2-1\"]/div[1]/div[2]/div/div/div/div[1]/div/div[2]/div/div[2]/div[2]/div[1]/table/tbody//td[@class=\"uitk-new-date-picker-day-number\"]/button[@data-day ='{0}']");
    public static final Target selectDayInitVuelos = Target.the("{0}").locatedBy("//*[@id=\"wizard-flight-tab-roundtrip\"]/div[2]/div[2]/div/div/div/div[1]/div/div[2]/div/div[2]/div[2]/div[1]/table/tbody//td[@class=\"uitk-new-date-picker-day-number\"]/button[@data-day ='{0}']");
    public static final Target btnDateInit = Target.the("date init").located(By.xpath("//button[@id='d1-btn']"));
    public static final Target btnDoneFechas = Target.the("btnDoneFechas").locatedBy("//*[@id=\"wizard-hotel-pwa-v2-1\"]/div[1]/div[2]/div/div/div/div[1]/div/div[2]/div/div[3]/button");
    public static final Target btnDoneFechasVuelos = Target.the("btnDoneFechas").locatedBy("//*[@id=\"wizard-flight-tab-roundtrip\"]/div[2]/div[2]/div/div/div/div[1]/div/div[2]/div/div[3]/button");

    public static final Target btnViajeros = Target.the("btnViajeros").locatedBy("//*[@id=\"adaptive-menu\"]/div[1]/button[1]");
    public static final Target btnViajerosVuelos = Target.the("btnViajeros").locatedBy("//*[@id=\"adaptive-menu\"]/a");

    public static final Target agregarAdulto = Target.the("agregarAdulto").locatedBy("//*[@id=\"adaptive-menu\"]/div[2]/div/section/div[1]/div[1]/div/button[2]");
    public static final Target agregarNino = Target.the("agregarNiño").locatedBy("//*[@id=\"adaptive-menu\"]/div[2]/div/section/div[1]/div[2]/div/button[2]");
    public static final Target agregarviajeroVuelo = Target.the("agregarviajeroVuelo").locatedBy("    //*[@id=\"adaptive-menu\"]/div/div/section/div[1]/div[1]/div/button[2]");
    public static final Target doneViajeros = Target.the("doneViajeros").locatedBy("//*[@id=\"adaptive-menu\"]/div[2]/div/div[2]/button");
    public static final Target doneViajerosVuelos = Target.the("doneViajerosVuelos").locatedBy("//*[@id=\"adaptive-menu\"]/div/div/div[2]/button");

    public static final Target buscar = Target.the("buscar").locatedBy("//button[contains(text(),'Search')]");
    public static final Target selectDestino = Target.the("selectDestino").locatedBy("//*[@id=\"location-field-destination-menu\"]/div[2]/ul/li[3]/button");
    public static final Target resultHotels = Target.the("selectDestino").locatedBy("//div[contains(text(), 'What we are paid impacts our sort order')]");
    public static final Target resultVuelo = Target.the("selectDestino").locatedBy("//span[contains(text(), 'Select your departure to Medellin')]");


    public static final Target btnLeavingFrom= Target.the("txtLeavingFrom").locatedBy("//*[@id=\"location-field-leg1-origin-menu\"]/div[1]/button");
    public static final Target selectLeavingFrom= Target.the("selectLeavingFrom").locatedBy("//*[@id=\"location-field-leg1-origin-menu\"]/div[2]/ul/li[1]");
    public static final Target btnGoingTo= Target.the("txtGoingTo").locatedBy("//*[@id=\"location-field-leg1-destination-menu\"]/div[1]/button");
    public static final Target selecGoingTo= Target.the("selecGoingTo").locatedBy("//*[@id=\"location-field-leg1-destination-menu\"]/div[2]/ul/li[1]");
    public static final Target btnBuscarVuelos= Target.the("btnBuscarVuelos").locatedBy("//*[@id=\"wizard-flight-pwa-1\"]/div[3]/div[2]/button");
    public static final Target btnVuelos= Target.the("btnVuelos").locatedBy("//*[@id=\"uitk-tabs-button-container\"]/li[2]/a");
    public static final Target txtLeavingFrom= Target.the("txtLeavingFrom").locatedBy("//*[@id=\"location-field-leg1-origin\"]");
    public static final Target txtGoingTo= Target.the("txtGoingTo").locatedBy("//*[@id=\"location-field-leg1-destination\"]");



}