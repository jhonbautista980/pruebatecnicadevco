package com.automation.qa.travelocity.tasks;

import com.automation.qa.travelocity.interations.SelectDay;
import com.automation.qa.travelocity.user_interfaces.TraveloCity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.thucydides.core.annotations.locators.WaitForWebElements;

public class ConsultaVuelos implements Task {
    private String leavingFrom ;
    private String goingTo;
    private String dayLeaving;
    private String dayGoingTo;
    private int viajeros;

    public ConsultaVuelos(String leavingFrom, String goingTo, String dayLeaving, String dayGoingTo,int viajeros) {
        this.leavingFrom = leavingFrom;
        this.goingTo = goingTo;
        this.dayLeaving = dayLeaving;
        this.dayGoingTo = dayGoingTo;
        this.viajeros= viajeros;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(TraveloCity.btnVuelos),
                Click.on(TraveloCity.btnViajerosVuelos));

        for (int i = 1; i < viajeros; i++) {
            actor.attemptsTo(Click.on(TraveloCity.agregarviajeroVuelo));
        }
        actor.attemptsTo(Click.on(TraveloCity.doneViajerosVuelos),Click.on(TraveloCity.btnDateInit),
                SelectDay.day(dayLeaving,"Vuelos"),SelectDay.day(dayGoingTo, "Vuelos"),
                Click.on(TraveloCity.btnDoneFechasVuelos),
                Click.on(TraveloCity.btnLeavingFrom),Click.on(TraveloCity.txtLeavingFrom),SendKeys.of(leavingFrom).into(TraveloCity.txtLeavingFrom),
                Click.on(TraveloCity.selectLeavingFrom),
                Click.on(TraveloCity.btnGoingTo),Click.on(TraveloCity.txtGoingTo),SendKeys.of(goingTo).into(TraveloCity.txtGoingTo),
                Click.on(TraveloCity.selecGoingTo),
                Click.on(TraveloCity.btnBuscarVuelos)
                );
    }

    public static  ConsultaVuelos consultaVuelos(String leavingFrom, String goingTo, String dayLeaving, String dayGoingTo,int viajeros){
        return Tasks.instrumented(ConsultaVuelos.class, leavingFrom,goingTo,dayLeaving,dayGoingTo,viajeros);
    }
}
