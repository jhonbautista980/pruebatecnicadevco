package com.automation.qa.travelocity.steps_definitions;

import com.automation.qa.travelocity.drivers.OwnWebDriver;
import com.automation.qa.travelocity.questions.Result;
import com.automation.qa.travelocity.questions.ResultVuelo;
import com.automation.qa.travelocity.tasks.ConsultaVuelos;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import org.hamcrest.Matchers;

public class ConsultaVuelosStep {


    @When("^Ingrese (.*) el (.*) en el (.*) y (.*) para (.*) adultos$")
    public void do_operation(String leavingFrom, String goingTo, String dayLeaving, String dayGoingTo,String viajeros) {
        OnStage.theActorInTheSpotlight().attemptsTo(ConsultaVuelos.consultaVuelos(leavingFrom, goingTo, dayLeaving, dayGoingTo,Integer.valueOf(viajeros)));

    }

    @Then("^Validar Vuelos   sugeridos (.*)$")
    public void result_is(String result) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(ResultVuelo.is_vuelo(), Matchers.equalTo(result)));

    }


}
