package com.automation.qa.travelocity.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(
        features = "src/test/resources/features/ConsultaVuelos.feature",
        glue = "com.automation.qa.travelocity.steps_definitions",
        snippets = SnippetType.CAMELCASE
)
public class ConsultaVuelosRunner {
}
