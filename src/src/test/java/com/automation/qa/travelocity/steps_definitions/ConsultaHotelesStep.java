package com.automation.qa.travelocity.steps_definitions;

import com.automation.qa.travelocity.drivers.OwnWebDriver;
import com.automation.qa.travelocity.questions.Result;
import com.automation.qa.travelocity.tasks.CosultaHoteles;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import org.hamcrest.Matchers;

public class ConsultaHotelesStep {

    @Given("^Abrir la aplicacion de travelocity$")
    public void open_travelocity() {
        OnStage.setTheStage(Cast.ofStandardActors());
        OnStage.theActorCalled("User");
        OnStage.theActorInTheSpotlight().can(BrowseTheWeb
                .with(OwnWebDriver.withChrome().setURL("https://www.travelocity.com/")));


    }
    @When("^Realizo el ingreso de (.*) la (.*) la (.*) los (.*) (.*) y (.*)$")
    public void do_operation(String destino , String diaInicio,String diaFin, String adultos, String ninos, String salidaDesde) {
        OnStage.theActorInTheSpotlight().attemptsTo(CosultaHoteles.cosulta(destino, diaInicio,diaFin, Integer.valueOf(adultos), Integer.valueOf(ninos), salidaDesde));

    }
    @Then("^Validar Destinos sugeridos (.*)$")
    public void result_is(String result) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Result.is(), Matchers.equalTo(result)));

    }
}
