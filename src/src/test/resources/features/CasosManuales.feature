Feature: Consulta para reservas
  Yo como usuario de la aplicacion web travleocity
  Quiero que pueda hacer consultas de los hoteles disponibles
  Para poder hacer consultas y reservas en destinos turisticos

  @Carros
  Scenario Outline: Consulta para reservas de Carros

    Given Abrir la aplicacion de travelocity
    When Realizo el ingreso de <lugarInicio> la <lugarDestino> la <diaInicio> <diaFin> a las <horaInicio> <horaFin>
    Then Validar Destinos sugeridos Total includes one-way drop-off charge

    Examples:
      | lugarInicio | lugarDestino | diaInicio | diaFin  | horaInicio | horaFin |
      | Bosa        | Kennedy      | Feb, 21   | Feb, 21 | 09:25      | 09:25   |
      | Kennedy     | Bosa         | Mar, 1    | Mar, 1  | 08:00      | 08:00   |
      | Suba        | Restrepo     | Feb, 1    | Feb, 1  | 12:30      | 12:30   |
      | Usme        | Santa Fe     | Jul, 1    | Jul, 1  | 12:30      | 12:30   |
      | Restrepo    | Kennedy      | Feb, 5    | Feb, 5  | 20:00      | 20:00   |

  @Paquetes
  Scenario Outline: Consulta para reservas de Paquetes

    Given Abrir la aplicacion de travelocity
    When Realizo el ingreso de <lugarInicio> la <lugarDestino> la <diaInicio> <diaFin> a las <horaInicio> <horaFin>
    Then Validar Destinos sugeridos Stay flexible with free cancellation

    Examples:
      | lugarInicio | lugarDestino | diaInicio | diaFin  |
      | Barcelona   | Italia       | Feb, 21   | Feb, 21 |
      | Grecia      | Roma         | Mar, 1    | Mar, 1  |
      | Argentina   | Chile        | Feb, 1    | Feb, 1  |
      | Colombia    | Mexico       | Jul, 1    | Jul, 1  |
      | Bogota      | Medellin     | Feb, 5    | Feb, 5  |


  @Creceros
  Scenario Outline: Consulta para reservas de Creceros

    Given Abrir la aplicacion de travelocity
    When Realizo el ingreso LugarDestino <lugarDestino> los <adultos> y <children> en la fecha <diaInicio> <diaFin>
    Then Validar Destinos sugeridos Cruise with confidence

    Examples:
      | lugarDestino | adultos | children | diaInicio | diaFin  |
      | Barcelona    | 2       | 0        | Feb, 21   | Mar, 21 |
      | Grecia       | 2       | 1        | Mar, 1    | Jul, 1  |
      | Argentina    | 2       | 2        | Feb, 1    | Feb, 20 |
      | Colombia     | 2       | 1        | Jun, 1    | Jul, 1  |
      | Bogota       | 2       | 1        | Feb, 5    | Feb, 28 |