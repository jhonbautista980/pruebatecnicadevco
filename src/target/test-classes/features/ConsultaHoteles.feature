Feature: Consulta para reservas
  Yo como usuario de la aplicacion web travleocity
  Quiero que pueda hacer consultas de los hoteles disponibles
  Para poder hacer consultas y reservas en destinos turisticos

  @Hoteles

  Scenario Outline: Consulta para reservas de hoteles

    Given Abrir la aplicacion de travelocity
    When Realizo el ingreso de <destino> la <diaInicio> la <diaFin> los <adultos> <ninos> y <salidaDesde>
    Then Validar Destinos sugeridos What we are paid impacts our sort order

    Examples:
      | destino   | diaInicio | diaFin | adultos | ninos | salidaDesde |
      | Barcelona | 6         | 8      | 4       | 2     | Bogota      |